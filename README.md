# Evo-Child-Vorlage für Shop 5.0

Das Repository für die Evo-Child-Vorlage wurde verschoben:
[Evo-Child-Vorlage auf Gitlab.com](https://gitlab.com/jtl-software/jtl-shop/child-templates/evo-child-vorlage)

## Versionen

- [Shop 5.0](https://gitlab.com/jtl-software/jtl-shop/child-templates/evo-child-vorlage/tree/master)
- [Shop 4.06](https://gitlab.com/jtl-software/jtl-shop/child-templates/evo-child-vorlage/tree/release/4.06)

## Related Links

- [Evo-Child-Vorlage für Version 4.06](https://gitlab.com/jtl-software/jtl-shop/child-templates/evo-child-vorlage/tree/release/4.06)
- [Evo-Child-Vorlage auf Gitlab.com](https://gitlab.com/jtl-software/jtl-shop/child-templates/evo-child-vorlage)
- [Templates](http://docs.jtl-shop.de/de/latest/shop_templates/index.html) - Entwickler Dokumentation Templates
